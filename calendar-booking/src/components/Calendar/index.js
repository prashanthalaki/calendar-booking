import React from "react";
import { withRouter } from "react-router-dom";
import { Card } from "antd";
class Calendar extends React.Component {
  render() {
    return (
      <div
        style={{
          display: "flex",
          flex: 1,
          height: "100%",
          width: "100%",
          flexDirection: "column",
        }}
      >
        <Card>Calendar</Card>
      </div>
    );
  }
}
export default withRouter(Calendar);
