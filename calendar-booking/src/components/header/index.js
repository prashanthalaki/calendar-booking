import React from "react";
import firebase from "firebase/app";
import { withRouter } from "react-router-dom";
import { Popover } from "antd";

class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userRecord: "",
      photoURL:
        "https://www.greenhandle.in/images/default_profile_greenhandle.png?%3E",
    };
  }
  componentDidMount() {
    this.loadUserInformation();
  }
  loadUserInformation = () => {
    let userRecord = localStorage.userRecord;
    if (userRecord) {
      try {
        userRecord = JSON.parse(userRecord);
        console.log(userRecord);
        this.setState({ userRecord });
      } catch (e) {
        alert(e);
      }
    }
  };
  logoutClick = () => {
    if (window.confirm("Are you sure you want logout ?")) {
      console.log("props", this.props);
      window.open("/login", "_self");
      localStorage.clear();
      firebase.auth().signOut();
    }
  };
  render() {
    let { userRecord, photoURL } = this.state;
    const content = (
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "flex-start",
          padding: "0 0 0 10px",
        }}
      >
        <span>Name : {userRecord.user && userRecord.user.displayName}</span>
        <span>Email : {userRecord.user && userRecord.user.email}</span>
        <span
          onClick={this.logoutClick}
          style={{
            border: "0.7px solid #ffff",
            padding: 10,
            cursor: "pointer",
            color: "#2879f2",
          }}
        >
          Logout
        </span>
      </div>
    );
    return (
      <div
        style={{
          display: "flex",
          height: 60,
          width: "100%",
          border: "1px solid red",
          background: "#2879f2",
          color: "#fafafa",
          alignItems: "center",
        }}
      >
        <div style={{ padding: 10, fontWeight: "bold" }}>Calender Booking</div>
        <div
          style={{
            display: "flex",
            flex: 1,
            justifyContent: "flex-end",
            padding: 10,
            alignItems: "center",
          }}
        >
          <Popover placement="bottomRight" content={content}>
            <img
              src={userRecord.user ? userRecord.user.photoURL : photoURL}
              style={{
                height: 50,
                borderRadius: "50%",
                border: "1px solid #fafafa",
              }}
              alt="profile"
            />
            <span>&#9660;</span>
          </Popover>
        </div>
      </div>
    );
  }
}
export default withRouter(Header);
