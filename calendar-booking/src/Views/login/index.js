import React from "react";
import firebase from "firebase/app";
import { IfFirebaseAuthed, FirebaseAuthConsumer } from "@react-firebase/auth";
import { Spin, message } from "antd";
import { withRouter } from "react-router-dom";
function Login(props) {
  const [isLoading, setIsLoading] = React.useState(false);
  const sendToHome = () => {
    props.history.push("/");
    message.success("Login successful.");
    setIsLoading(false);
    return "";
  };
  return (
    <div
      style={{
        display: "flex",
        flex: 1,
        height: "100%",
        width: "100%",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <Spin tip="Loading..." spinning={isLoading} delay={500}>
        <div
          style={{
            height: 50,
            width: 300,
            justifyContent: "center",
            alignItems: "center",
            display: "flex",
            flexDirection: "row",
            cursor: "pointer",
          }}
          onClick={() => {
            setIsLoading(true);
            const googleAuthProvider = new firebase.auth.GoogleAuthProvider();
            firebase.auth().signInWithPopup(googleAuthProvider);
          }}
        >
          <div
            style={{
              width: 70,
              border: "1px solid #2879f2",
              height: "100%",
              textAlign: "center",
              justifyContent: "center",
              alignItems: "center",
              display: "flex",
            }}
          >
            <img
              src={require("../../assets/images/google.png")}
              style={{ height: 30 }}
              alt="google"
            />
          </div>
          <div
            style={{
              width: 200,
              border: "1px solid #2879f2",
              height: "100%",
              textAlign: "center",
              justifyContent: "center",
              alignItems: "center",
              display: "flex",
              background: "#2879f2",
              color: "white",
              fontWeight: "bold",
            }}
          >
            Sign in with Google
          </div>
        </div>
        <FirebaseAuthConsumer>
          {({ isSignedIn, user, providerId }) => {
            localStorage.setItem(
              "userRecord",
              JSON.stringify({ isSignedIn, user, providerId }, null, 2)
            );
          }}
        </FirebaseAuthConsumer>
        <IfFirebaseAuthed>
          {() => {
            return <div>{sendToHome()}</div>;
          }}
        </IfFirebaseAuthed>
      </Spin>
    </div>
  );
}
export default withRouter(Login);
