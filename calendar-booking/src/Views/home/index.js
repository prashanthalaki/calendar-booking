import React from "react";
import { withRouter } from "react-router-dom";
import HeaderBar from "../../components/header/index";
import Calendar from "../../components/Calendar/index";
class Home extends React.Component {
  render() {
    return (
      <div
        style={{
          display: "flex",
          flex: 1,
          height: "100%",
          width: "100%",
          flexDirection: "column",
        }}
      >
        <HeaderBar />
        <div>
          <div>12</div>
          <Calendar />
        </div>
      </div>
    );
  }
}
export default withRouter(Home);
