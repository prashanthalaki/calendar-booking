import React from "react";
import { Router, Route, Switch } from "react-router-dom";
import { createBrowserHistory } from "history";
import firebase from "firebase/app";
import "firebase/auth";
import { firebaseConfig as config } from "./core/firebase/index";
import { FirebaseAuthProvider } from "@react-firebase/auth";
import "./App.css";
import Login from "./Views/login/index";
import Home from "./Views/home/index";
import "antd/dist/antd.css";
const history = createBrowserHistory();
function App() {
  React.useEffect(() => {
    let userRecord = localStorage.userRecord;
    if (userRecord.user) {
      history.push("/");
    } else {
      history.push("/login");
    }
  }, []);
  return (
    <div className="App">
      <FirebaseAuthProvider {...config} firebase={firebase}>
        <Router history={history}>
          <Switch>
            <Route path="/login">
              <Login />
            </Route>
            <Route path="/">
              <Home />
            </Route>
          </Switch>
        </Router>
      </FirebaseAuthProvider>
    </div>
  );
}

export default App;
